from flask import Flask, request, jsonify, Blueprint
from errors import InternalServerError
from database import (
    get_all_images_db,
    get_image_db,
    insert_image_db,
    create_table,
    delete_image_db,
)
from image_reducer import reduce_image_size, delete_file


app = Flask(__name__)


@app.errorhandler(InternalServerError)
def handle_internal_server_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route("/api/v1/all-images", methods=["GET"])
def get_all_images():
    try:
        limit = request.args.get("limit", default=5, type=int)
        order = request.args.get("order", default="desc", type=str)
        all_images = get_all_images_db(limit, order)
        if all_images is None:
            return jsonify({"message": "There are no lists"}), 404
        return jsonify({"all_images": all_images}), 200
    except Exception as e:
        error_msg = str(e)
        raise InternalServerError(error_msg, 500)


@app.route("/api/v1/resize-image/<image_id>", methods=["DELETE"])
def delete_existing_image(image_id):
    try:
        is_deleted = delete_image_db(image_id)
        if is_deleted:
            return jsonify({"message": "Successfully delete the record"}), 204
        return (
            jsonify({"message": "Unable to delete the record with uncertain reason"}),
            404,
        )
    except Exception as e:
        error_msg = str(e)
        raise InternalServerError(error_msg, 500)


@app.route("/api/v1/resize-image/<image_id>", methods=["GET"])
def get_resize_image(image_id):
    try:
        get_image = get_image_db(image_id=image_id)
        if not get_image:
            return (
                jsonify({"message": "Couldn't fetch that image based on the ID"}),
                404,
            )

        input_image_response = get_image[1]
        output_image_response = get_image[2]
        target_size_response = get_image[3]
        quality_response = get_image[4]

        reduce_img, _, new_file_size = reduce_image_size(
            input_image_response,
            output_image_response,
            target_size_response,
            quality_response,
        )

        if reduce_img:
            # file = send_file(output_image_response, as_attachment=True)
            return jsonify(
                {
                    "filename": output_image_response,
                    "new_size": f"{new_file_size:.2f} MB",
                }
            )

        return jsonify({"message": "Failed to resize the image"}), 422
    except Exception as e:
        error_msg = str(e)
        raise InternalServerError(error_msg, 500)


@app.route("/api/v1/resize-image", methods=["POST"])
def resize_image():
    try:
        request_json = request.get_json()
        input_image_request = request_json["input_path"]
        output_image_request = request_json["output_path"]
        target_size_request = int(request_json["target_size"])
        quality_request = int(request_json["quality"])
        is_optimize_request = request_json["is_optimize"]
        is_resize_request = request_json["is_resize"]

        reduce_img, _, get_new_size = reduce_image_size(
            input_image_request,
            output_image_request,
            target_size_request,
            quality_request,
            is_optimize_request,
            is_resize_request,
        )

        insert_image_db(
            input_image_request,
            output_image_request,
            target_size_request,
            quality_request,
        )

        if reduce_img:
            response = {
                "message": "Image successfully saved and reduced the size",
                "new_size": f"{get_new_size:.2f} MB",
                "is_optimized": is_optimize_request,
            }
        else:
            response = {"message": "Failed to resize the image"}, 500
        return jsonify(response), 201
    except Exception as e:
        error_msg = str(e)
        raise InternalServerError(error_msg, 500)


if __name__ == "__main__":
    create_table()
    app.run(debug=True)
