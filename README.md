## image reducer

Yet another library to compress the given image with desired output size. It's also having a (simple) REST services to expose the APIs so it can be used if you want to integrate with the client-side

## Why?

Why make something infuriating like this? honestly.... i stumbled upon a situation some time ago where i needed to convert or compress my image file related to, you know, several things that required to such as photos for passport purposes, academic purposes, work-related and so on. Actually, there are lots of websites out there to overcome out this situation, but i've some urgency (and concern) matters regarding that:

- Data and privacy
- Simplicity -> at least for me
- curiosity

Thus, i got the idea to make something like this

## Example

If you don't want to use the REST endpoints that already provided, you might want to use it directly like :

```python
input_img_path = "your image path or name"
output_img_path = "expected image path"
target_size = 1500 # expected output size
quality = 70 # set the quality for the compression

reduce_img = reduce_image_size(input_img_path, output_img_path, target_size, quality)
```

Otherwise, you can also leveraging the REST services by hit on the particular endpoints such as :

[POST] `http://127.0.0.1:5000/api/v1/resize-image`

```json
/*request payload to reduce the image size*/
{
	"input_path": "filename.png", 
	"output_path": "resize_filename.png", 
	"target_size": 990, 
	"quality": 85,
	"is_optimize": true,  // enable the optimize parameter
	"is_resize": true  // resize the original image
}
```

## Caveats

Actually, the reduction of image size itself wasn't too great at the time being, it's only reduced up-to 30-38 % from the original size. Basically it was using the established encoding from `PIL` libraries and if you want to go lower size, you may need more tweaking and adjustments on how to make it right. For example, you think need to configure between the `quality` and `target_size` parameters
 