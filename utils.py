import logging


def log(message: str, log_level: int = None) -> None:
    logger = logging.getLogger("image_app_logs")
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(
        logging.Formatter(
            fmt="%(asctime)s : %(filename)s : %(funcName)s : %(message)s",
            datefmt="%d-%m-%Y %I:%M:%S",
        )
    )
    logger.addHandler(log_handler)

    if log_level is None:
        logger.setLevel(level=logging.DEBUG)
    else:
        logger.setLevel(level=log_level)
    logger.propagate = False
    logger.info(message)
