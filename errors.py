class InternalServerError(Exception):
    status_code = 500

    def __init__(self, message: str, status_code: int = None) -> None:
        Exception.__init__(self)
        if status_code is not None:
            self.status_code = status_code
        self.message = message

    def to_dict(self) -> dict:
        return_value = dict()
        return_value["message"] = self.message
        return return_value